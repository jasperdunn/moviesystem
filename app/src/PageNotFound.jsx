import React from 'react'
import PropTypes from 'prop-types'
import Page from './shared/Page'

const PageNotFound = props => (
  <Page>
    <h1>Page not found</h1>
    <h2>
      No match for <code>{props.location.pathname}</code>
    </h2>
  </Page>
)

PageNotFound.propTypes = {
  location: PropTypes.string.isRequired
}

export default PageNotFound
