import React from 'react'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import MoviesPage from './movies/MoviesPage'
import MoviePage from './movies/MoviePage'
import MovieCreate from './movies/MovieCreate'
import MovieUpdate from './movies/MovieUpdate'
import DirectorsPage from './directors/DirectorsPage'
import DirectorPage from './directors/DirectorPage'
import DirectorCreate from './directors/DirectorCreate'
import DirectorUpdate from './directors/DirectorUpdate'
import ActorsPage from './actors/ActorsPage'
import ActorPage from './actors/ActorPage'
import ActorCreate from './actors/ActorCreate'
import ActorUpdate from './actors/ActorUpdate'
import PageNotFound from './PageNotFound'

const Main = props => (
  <StyledMain>
    <Switch>
      <Route exact strict path="/" component={MoviesPage} />
      <Route exact strict path="/movies" component={MoviesPage} />
      <Route exact strict path="/movies/create" component={MovieCreate} />
      <Route
        exact
        strict
        path="/movies/:movieId/update"
        component={MovieUpdate}
      />
      <Route exact strict path="/movies/:movieId" component={MoviePage} />

      <Route exact strict path="/directors" component={DirectorsPage} />
      <Route exact strict path="/directors/create" component={DirectorCreate} />
      <Route
        exact
        strict
        path="/directors/:directorId/update"
        component={DirectorUpdate}
      />
      <Route
        exact
        strict
        path="/directors/:directorId"
        component={DirectorPage}
      />

      <Route exact strict path="/actors" component={ActorsPage} />
      <Route exact strict path="/actors/create" component={ActorCreate} />
      <Route
        exact
        strict
        path="/actors/:actorId/update"
        component={ActorUpdate}
      />
      <Route exact strict path="/actors/:actorId" component={ActorPage} />

      <Route component={PageNotFound} />
    </Switch>
  </StyledMain>
)

const StyledMain = styled.main`
  padding-top: 48px;
`

export default Main
