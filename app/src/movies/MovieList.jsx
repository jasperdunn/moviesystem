import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MovieListItem from './MovieListItem'
import LoadingSpinner from '../shared/LoadingSpinner'
import { getAll } from './MovieDataAccess'
import List from '../shared/List'

class MovieList extends Component {
  state = {
    movies: null
  }

  renderMovies = () => (
    <List>
      {this.state.movies.map(movie => {
        return (
          <MovieListItem
            movie={movie}
            key={movie.movieId}
            match={this.props.match}
          />
        )
      })}
    </List>
  )

  componentDidMount() {
    getAll()
      .then(response => {
        this.setState({
          movies: response.data
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.movies === null) {
      return <LoadingSpinner />
    }
    return this.renderMovies()
  }
}

MovieList.propTypes = {
  match: PropTypes.object.isRequired
}

export default MovieList
