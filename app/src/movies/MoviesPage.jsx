import React from 'react'
import PropTypes from 'prop-types'
import MovieList from './MovieList'
import AddLink from '../shared/AddLink'
import Page from '../shared/Page'
import ResponsiveDiv from '../shared/ResponsiveDiv'

const MoviesPage = props => {
  return (
    <Page title="Movies">
      <ResponsiveDiv>
        <AddLink to={`${props.match.url}/create`} />
        <MovieList match={props.match} />
      </ResponsiveDiv>
    </Page>
  )
}

MoviesPage.propTypes = {
  match: PropTypes.object.isRequired
}

export default MoviesPage
