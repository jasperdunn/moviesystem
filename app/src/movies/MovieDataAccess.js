import axios from 'axios'

const endPoint = process.env.REACT_APP_API_ENDPOINT

const url = `${endPoint}/movies`

export const getAll = () => {
  return axios.get(url).catch(error => console.log(error))
}

export const getSingle = movieId => {
  return axios.get(`${url}/${movieId}`).catch(error => console.log(error))
}

export const create = movie => {
  return axios
    .post(url, {
      title: movie.title,
      year: movie.year,
      genre: movie.genre,
      imdbId: movie.imdbId
    })
    .catch(error => console.log(error))
}

export const update = movie => {
  return axios
    .put(url, {
      movieId: movie.movieId,
      title: movie.title,
      year: movie.year,
      genre: movie.genre,
      imdbId: movie.imdbId
    })
    .catch(error => console.log(error))
}

export const del = movieId => {
  return axios.delete(`${url}/${movieId}`).catch(error => console.log(error))
}
