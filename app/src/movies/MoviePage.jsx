import React, { Component } from 'react'
import Page from '../shared/Page'
import ResponsiveDiv from '../shared/ResponsiveDiv'
import { getSingle } from './MovieDataAccess'

class MoviePage extends Component {
  state = {
    movie: null
  }

  componentDidMount() {
    getSingle(this.props.match.params.movieId)
      .then(response => {
        this.setState({
          movie: response.data
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.movie === null) {
      return null
    }
    return (
      <Page title={this.state.movie.title}>
        <ResponsiveDiv>
          <h1>{this.state.movie.title}</h1>
          <p>Year: {this.state.movie.year}</p>
          {this.state.movie.genre && <p>Genre: {this.state.movie.genre}</p>}
          {this.state.movie.imdbId && (
            <a href={`https://www.imdb.com/title/${this.state.movie.imdbId}/`}>
              IMDB
            </a>
          )}
          {this.state.movie.rating && <p>Rating: {this.state.movie.rating}</p>}
          {this.state.movie.runtime && (
            <p>Runtime: {this.state.movie.runtime}</p>
          )}
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default MoviePage
