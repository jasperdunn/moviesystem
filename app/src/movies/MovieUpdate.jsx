import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { getSingle, update, del } from './MovieDataAccess'
import { getAll as getAllDirectors } from '../directors/DirectorDataAccess'
import Button from '../shared/Button'
import TextFieldWithLabel from '../shared/TextFieldWithLabel'
import Page from '../shared/Page'
import Form from '../shared/Form'
import ResponsiveDiv from '../shared/ResponsiveDiv'
import SelectWithLabel from '../shared/SelectWithLabel'

class MovieUpdate extends Component {
  state = {
    title: '',
    year: new Date().getUTCFullYear(),
    genre: '',
    imdbId: '',
    directors: null,
    director: null,
    redirect: false
  }

  componentDidMount() {
    getSingle(this.props.match.params.movieId)
      .then(response => {
        this.setState({
          title: response.data.title,
          year: response.data.year,
          genre: response.data.genre,
          imdbId: response.data.imdbId
        })
      })
      .catch(error => console.log(error))

    getAllDirectors()
      .then(response => {
        if (response.data.length > 0) {
          var directors = response.data.map(d => ({
            key: d.directorId,
            value: d.name
          }))

          this.setState({
            directors,
            director: directors[0]
          })
        }
      })
      .catch(error => console.log(error))
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  onSaveChanges = event => {
    event.preventDefault()
    const movie = {
      movieId: this.props.match.params.movieId,
      title: this.state.title,
      year: this.state.year,
      genre: this.state.genre,
      imdbId: this.state.imdbId,
      directorId: this.state.director
    }
    update(movie)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  onDelete = event => {
    event.preventDefault()
    del(this.props.match.params.movieId)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.title === null) {
      return null
    }
    if (this.state.redirect) {
      return <Redirect push to="/movies" />
    }
    return (
      <Page title="Update Movie">
        <ResponsiveDiv>
          <Form onSubmit={this.onSaveChanges} title="Update Movie">
            <TextFieldWithLabel
              name="title"
              label="Title"
              value={this.state.title}
              onChange={this.onChange}
              required
            />
            <TextFieldWithLabel
              name="year"
              label="Year"
              value={this.state.year}
              onChange={this.onChange}
              type="number"
              required
            />
            {this.state.directors && (
              <SelectWithLabel
                name="director"
                label="Director"
                value={this.state.director}
                options={this.state.directors}
                onChange={this.onChange}
                required
              />
            )}
            <TextFieldWithLabel
              name="genre"
              label="Genre"
              value={this.state.genre}
              onChange={this.onChange}
            />
            <TextFieldWithLabel
              name="imdbId"
              label="IMDB Id"
              value={this.state.imdbId}
              onChange={this.onChange}
            />
            <Link to="/movies">Cancel</Link>
            <Button type="submit">Update</Button>
            <Button type="button" onClick={this.onDelete}>
              Delete
            </Button>
          </Form>
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default MovieUpdate
