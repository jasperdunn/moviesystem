import React from 'react'
import PropTypes from 'prop-types'
import color from './color'

const Icon = props => (
  <svg
    fill={props.color}
    height={props.size}
    viewBox="0 0 24 24"
    width={props.size}
    xmlns="http://www.w3.org/2000/svg"
  >
    <Path type={props.type} />
  </svg>
)

Icon.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  type: PropTypes.string.isRequired
}

Icon.defaultProps = {
  color: color.medium,
  size: 48
}

const Path = props => {
  switch (props.type) {
    case 'add':
      return (
        <path d="M13 7h-2v4H7v2h4v4h2v-4h4v-2h-4V7zm-1-5C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z" />
      )
    case 'edit':
      return (
        <path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z" />
      )
    case 'delete':
      return (
        <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
      )
    default:
      return null
  }
}

Path.propTypes = {
  type: PropTypes.string.isRequired
}

export default Icon
