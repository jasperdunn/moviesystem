import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import color from './color'

const Form = props => (
  <StyledForm onSubmit={props.onSubmit}>
    <FieldSet>
      <legend>{props.title}</legend>
      {props.children}
    </FieldSet>
  </StyledForm>
)

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
}

const StyledForm = styled.form`
  color: ${color.mediumDark};
`

const FieldSet = styled.fieldset`
  border: 3px solid ${color.lighter};
  border-radius: 10px;
`

export default Form
