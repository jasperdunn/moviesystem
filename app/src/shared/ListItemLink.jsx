import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import color from '../shared/color'

const ListItemLink = props => (
  <StyledLink to={props.to}>{props.children}</StyledLink>
)

const StyledLink = styled(Link)`
  background-color: white;
  text-decoration: none;
  display: block;
  margin-bottom: 16px;
  border-radius: 5px;
  box-shadow: 0 2px 4px 0 hsla(0, 0%, 0%, 0.2);
  &:hover {
    background-color: ${color.almostWhite};
  }
`

ListItemLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}

export default ListItemLink
