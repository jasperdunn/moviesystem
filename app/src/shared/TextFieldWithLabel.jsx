import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { media } from '../shared/mediaQueries'

const TextFieldWithLabel = props => (
  <p>
    <Label htmlFor={props.name} title={props.required && 'required'}>
      {props.label}
      {props.required && <span> *</span>}
    </Label>
    <Input
      autoComplete={props.autoComplete}
      id={props.name}
      name={props.name}
      onChange={props.onChange}
      placeholder={props.placeholder}
      required={props.required}
      type={props.type}
      value={props.value}
    />
  </p>
)

TextFieldWithLabel.propTypes = {
  autoComplete: PropTypes.string,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.oneOf(['text', 'password', 'email', 'number']).isRequired,
  value: PropTypes.any
}

TextFieldWithLabel.defaultProps = {
  type: 'text'
}

// shouldn't have width

const Input = styled.input`
  border: none;
  border-radius: 5px;
  padding: 5px;
  width: 100%;
  ${media.landscapeMobile`
    width: 400px;
  `};
`

const Label = styled.label`
  padding-right: 6px;
  float: left;
  ${media.landscapeMobile`
    float: none;
  `};
`

export default TextFieldWithLabel
