import styled from 'styled-components'
import color from './color'

const Button = styled.button`
  margin: 2px;
  border: none;
  border-radius: 5px;
  padding: 10px;
  cursor: pointer;
  color: ${color.mediumDark};
  background-color: ${color.lighter};
  :hover {
    color: white;
    background-color: ${color.medium};
  }
`

export default Button
