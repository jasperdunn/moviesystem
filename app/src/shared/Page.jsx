import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Page = props => {
  return (
    <Div>
      <Helmet>
        <title>{props.title}</title>
      </Helmet>
      {props.children}
    </Div>
  )
}

Page.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node
}

const Div = styled.div`
  padding: 16px;
`

export default Page
