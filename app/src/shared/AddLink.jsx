import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Icon from './Icon'
import color from './color'

const AddLink = props => (
  <StyledLink to={props.to} aria-label="Add link">
    <Icon type="add" color={color.light} />
  </StyledLink>
)

AddLink.propTypes = {
  to: PropTypes.string.isRequired
}

const StyledLink = styled(Link)`
  height: 48px;
  width: 48px;
  margin: auto;
  margin-bottom: 16px;
  display: block;
  &:hover svg {
    fill: ${color.medium};
  }
`

export default AddLink
