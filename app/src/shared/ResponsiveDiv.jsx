import styled from 'styled-components'
import { media } from '../shared/mediaQueries'

const ResponsiveDiv = styled.div`
  ${media.tablet`
  width: 66%;
  margin: auto;
`};
  ${media.largeDesktop`
  width: 50%;
  margin: auto;
`};
`

export default ResponsiveDiv
