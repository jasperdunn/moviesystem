import { css } from 'styled-components'

const sizes = {
  landscapeMobile: 568,
  tablet: 768,
  desktop: 992,
  largeDesktop: 1200
}

// Iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label] / 16}em) {
      ${css(...args)};
    }
  `
  return acc
}, {})
