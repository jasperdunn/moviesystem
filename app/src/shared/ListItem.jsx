import styled from 'styled-components'

const ListItem = styled.li`
  list-style: none;
  position: relative;
`

export default ListItem
