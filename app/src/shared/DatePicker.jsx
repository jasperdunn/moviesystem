import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { browserHasInputType } from './featureDetector'

const DatePicker = props => {
  return (
    <p>
      <Label htmlFor="time" title="required">
        {props.label}
        <span> *</span>
      </Label>
      {browserHasInputType('date') ? (
        <Input
          type="date"
          name={props.name}
          value={props.value}
          onChange={props.onChange}
          required
        />
      ) : (
        <Input
          name={props.name}
          type="text"
          title="yyyy-mm-dd"
          placeholder="yyyy-mm-dd"
          defaultValue={props.value}
          pattern="[0-9]{4}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"
          onBlur={props.onChange}
          required
        />
      )}
    </p>
  )
}

DatePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

const Input = styled.input`
  font-family: monospace;
  border: none;
  border-radius: 5px;
  padding: 5px;
  width: 150px;
`

const Label = styled.label`
  padding-right: 6px;
`

export default DatePicker
