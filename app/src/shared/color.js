const color = {
  darkest: 'hsl(0, 0%, 10%)',
  darker: 'hsl(0, 0%, 20%)',
  dark: 'hsl(0, 0%, 30%)',
  mediumDark: 'hsl(0, 0%, 40%)',
  medium: 'hsl(0, 0%, 50%)',
  mediumLight: 'hsl(0, 0%, 60%)',
  light: 'hsl(0, 0%, 70%)',
  lighter: 'hsl(0, 0%, 80%)',
  lighterStill: 'hsl(0, 0%, 85%)',
  lightest: 'hsl(0, 0%, 90%)',
  almostWhite: 'hsl(0, 0%, 95%)'
}

export default color
