export const browserHasInputType = type => {
  var input = document.createElement('input')
  input.setAttribute('type', type)
  return input.type === type
}
