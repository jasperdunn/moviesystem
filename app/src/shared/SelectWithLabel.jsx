import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { media } from '../shared/mediaQueries'

const SelectWithLabel = props => (
  <p>
    <Label htmlFor={props.name} title={props.required && 'required'}>
      {props.label}
      {props.required && <span> *</span>}
    </Label>
    <Select name={props.name} onChange={props.onChange} value={props.value}>
      {props.options.map(option => (
        <option key={option.key} value={option.key}>
          {option.value}
        </option>
      ))}
    </Select>
  </p>
)

SelectWithLabel.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool
}

const Label = styled.label`
  padding-right: 6px;
  float: left;
  ${media.landscapeMobile`
    float: none;
  `};
`

const Select = styled.select`
  width: 100%;
  ${media.landscapeMobile`
    width: 400px;
  `};
`

export default SelectWithLabel
