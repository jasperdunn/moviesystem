import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { create } from './DirectorDataAccess'
import Button from '../shared/Button'
import TextFieldWithLabel from '../shared/TextFieldWithLabel'
import DatePicker from '../shared/DatePicker'
import Page from '../shared/Page'
import Form from '../shared/Form'
import ResponsiveDiv from '../shared/ResponsiveDiv'

class DirectorCreate extends Component {
  state = {
    name: '',
    dateOfBirth: new Date().toISOString().split('T')[0],
    redirect: false
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  onCreate = event => {
    event.preventDefault()
    const director = {
      name: this.state.name,
      dateOfBirth: this.state.dateOfBirth
    }
    create(director)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/directors" />
    }
    return (
      <Page title="Create Director">
        <ResponsiveDiv>
          <Form onSubmit={this.onCreate} title="Create Director">
            <TextFieldWithLabel
              name="name"
              label="Name"
              value={this.state.name}
              onChange={this.onChange}
              required
            />
            <DatePicker
              name="dateOfBirth"
              label="Date of birth"
              value={this.state.dateOfBirth}
              onChange={this.onChange}
            />
            <Link to="/directors">Cancel</Link>
            <Button type="submit">Create</Button>
          </Form>
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default DirectorCreate
