import axios from 'axios'

const endPoint = process.env.REACT_APP_API_ENDPOINT

const url = `${endPoint}/directors`

export const getAll = () => {
  return axios.get(url).catch(error => console.log(error))
}

export const getSingle = directorId => {
  return axios.get(`${url}/${directorId}`).catch(error => console.log(error))
}

export const create = director => {
  return axios
    .post(url, {
      name: director.name,
      dateOfBirth: director.dateOfBirth
    })
    .catch(error => console.log(error))
}

export const update = director => {
  return axios
    .put(url, {
      directorId: director.directorId,
      name: director.name,
      dateOfBirth: director.dateOfBirth
    })
    .catch(error => console.log(error))
}

export const del = directorId => {
  return axios.delete(`${url}/${directorId}`).catch(error => console.log(error))
}
