import React from 'react'
import PropTypes from 'prop-types'
import DirectorList from './DirectorList'
import AddLink from '../shared/AddLink'
import Page from '../shared/Page'
import ResponsiveDiv from '../shared/ResponsiveDiv'

const DirectorsPage = props => {
  return (
    <Page title="Directors">
      <ResponsiveDiv>
        <AddLink to={`${props.match.url}/create`} />
        <DirectorList match={props.match} />
      </ResponsiveDiv>
    </Page>
  )
}

DirectorsPage.propTypes = {
  match: PropTypes.object.isRequired
}

export default DirectorsPage
