import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { getSingle, update, del } from './DirectorDataAccess'
import Button from '../shared/Button'
import TextFieldWithLabel from '../shared/TextFieldWithLabel'
import DatePicker from '../shared/DatePicker'
import Page from '../shared/Page'
import Form from '../shared/Form'
import ResponsiveDiv from '../shared/ResponsiveDiv'

class DirectorUpdate extends Component {
  state = {
    name: null,
    dateOfBirth: null,
    redirect: false
  }

  componentDidMount() {
    getSingle(this.props.match.params.directorId)
      .then(response => {
        this.setState({
          name: response.data.name,
          dateOfBirth: response.data.dateOfBirth
        })
      })
      .catch(error => console.log(error))
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  onSaveChanges = event => {
    event.preventDefault()
    const director = {
      directorId: this.props.match.params.directorId,
      name: this.state.name,
      dateOfBirth: this.state.dateOfBirth
    }
    update(director)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  onDelete = event => {
    event.preventDefault()
    del(this.props.match.params.directorId)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.name === null) {
      return null
    }
    if (this.state.redirect) {
      return <Redirect push to="/directors" />
    }
    return (
      <Page title="Update Director">
        <ResponsiveDiv>
          <Form onSubmit={this.onSaveChanges} title="Update Director">
            <TextFieldWithLabel
              name="name"
              label="Name"
              value={this.state.name}
              onChange={this.onChange}
              required
            />
            <DatePicker
              name="dateOfBirth"
              label="Date of birth"
              value={this.state.dateOfBirth}
              onChange={this.onChange}
            />
            <Link to="/directors">Cancel</Link>
            <Button type="submit">Save Changes</Button>
            <Button type="button" onClick={this.onDelete}>
              Delete
            </Button>
          </Form>
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default DirectorUpdate
