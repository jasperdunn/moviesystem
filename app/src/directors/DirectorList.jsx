import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DirectorListItem from './DirectorListItem'
import LoadingSpinner from '../shared/LoadingSpinner'
import { getAll } from './DirectorDataAccess'
import List from '../shared/List'

class DirectorList extends Component {
  state = {
    directors: null
  }

  renderDirectors = () => (
    <List>
      {this.state.directors.map(director => {
        return (
          <DirectorListItem
            director={director}
            key={director.directorId}
            match={this.props.match}
          />
        )
      })}
    </List>
  )

  componentDidMount() {
    getAll()
      .then(response => {
        this.setState({
          directors: response.data
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.directors === null) {
      return <LoadingSpinner />
    }
    return this.renderDirectors()
  }
}

DirectorList.propTypes = {
  match: PropTypes.object.isRequired
}

export default DirectorList
