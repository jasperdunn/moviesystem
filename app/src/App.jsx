import React, { Component, Fragment } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { injectGlobal } from 'styled-components'
import Helmet from 'react-helmet'
import Nav from './Nav'
import Main from './Main'
import color from './shared/color'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Helmet
            defaultTitle="Movie System"
            titleTemplate="%s | Movie System"
          />
          <Nav />
          <Main />
        </Fragment>
      </BrowserRouter>
    )
  }
}

injectGlobal`
    html {
    box-sizing: border-box;
    min-height: 100%;
    background-color: ${color.lightest};
  }
  *, :before, :after {
    box-sizing: inherit;
  }
  body {
    margin: 0;
    font-family: 'Source Sans Pro', sans-serif;
  }
`

export default App
