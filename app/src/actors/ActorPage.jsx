import React, { Component } from 'react'
import Page from '../shared/Page'
import ResponsiveDiv from '../shared/ResponsiveDiv'
import { getSingle } from './ActorDataAccess'

class ActorPage extends Component {
  state = {
    name: null,
    dateOfBirth: null
  }

  componentDidMount() {
    getSingle(this.props.match.params.actorId)
      .then(response => {
        this.setState({
          name: response.data.name,
          dateOfBirth: new Date(response.data.dateOfBirth)
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.name === null) {
      return null
    }
    return (
      <Page title="Actor">
        <ResponsiveDiv>
          <h1>{this.state.name}</h1>
          <p>
            Date of birth: {this.state.dateOfBirth.toISOString().slice(0, 10)}
          </p>
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default ActorPage
