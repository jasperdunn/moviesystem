import React from 'react'
import PropTypes from 'prop-types'
import ActorList from './ActorList'
import AddLink from '../shared/AddLink'
import Page from '../shared/Page'
import ResponsiveDiv from '../shared/ResponsiveDiv'

const ActorsPage = props => {
  return (
    <Page title="Actors">
      <ResponsiveDiv>
        <AddLink to={`${props.match.url}/create`} />
        <ActorList match={props.match} />
      </ResponsiveDiv>
    </Page>
  )
}

ActorsPage.propTypes = {
  match: PropTypes.object.isRequired
}

export default ActorsPage
