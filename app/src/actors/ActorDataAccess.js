import axios from 'axios'

const endPoint = process.env.REACT_APP_API_ENDPOINT

const url = `${endPoint}/actors`

export const getAll = () => {
  return axios.get(url).catch(error => console.log(error))
}

export const getSingle = actorId => {
  return axios.get(`${url}/${actorId}`).catch(error => console.log(error))
}

export const create = actor => {
  return axios
    .post(url, {
      name: actor.name,
      dateOfBirth: actor.dateOfBirth
    })
    .catch(error => console.log(error))
}

export const update = actor => {
  return axios
    .put(url, {
      actorId: actor.actorId,
      name: actor.name,
      dateOfBirth: actor.dateOfBirth
    })
    .catch(error => console.log(error))
}

export const del = actorId => {
  return axios.delete(`${url}/${actorId}`).catch(error => console.log(error))
}
