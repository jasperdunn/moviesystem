import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import ListItem from '../shared/ListItem'
import ListItemLink from '../shared/ListItemLink'
import color from '../shared/color'
import Icon from '../shared/Icon'

const ActorListItem = props => (
  <ListItem>
    <EditLink
      to={`${props.match.url}/${props.actor.actorId}/update`}
      aria-label="edit"
    >
      <Icon type="edit" color={color.lighter} size={36} />
    </EditLink>
    <ListItemLink to={`${props.match.url}/${props.actor.actorId}`}>
      <Text>{props.actor.name}</Text>
    </ListItemLink>
  </ListItem>
)

ActorListItem.propTypes = {
  match: PropTypes.object.isRequired,
  actor: PropTypes.object.isRequired
}

const Text = styled.div`
  color: black;
  font-size: 20px;
  padding: 5px 87px 20px 5px;
`

const EditLink = styled(Link)`
  display: block;
  position: absolute;
  height: 36px;
  width: 36px;
  border-radius: 5px;
  margin: 5px;
  right: 0;

  &:hover {
    background-color: ${color.lightest};
  }

  &:hover svg {
    fill: ${color.medium};
  }
`

export default ActorListItem
