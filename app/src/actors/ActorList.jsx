import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ActorListItem from './ActorListItem'
import LoadingSpinner from '../shared/LoadingSpinner'
import { getAll } from './ActorDataAccess'
import List from '../shared/List'

class ActorList extends Component {
  state = {
    actors: null
  }

  renderActors = () => (
    <List>
      {this.state.actors.map(actor => {
        return (
          <ActorListItem
            actor={actor}
            key={actor.actorId}
            match={this.props.match}
          />
        )
      })}
    </List>
  )

  componentDidMount() {
    getAll()
      .then(response => {
        this.setState({
          actors: response.data
        })
      })
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.actors === null) {
      return <LoadingSpinner />
    }
    return this.renderActors()
  }
}

ActorList.propTypes = {
  match: PropTypes.object.isRequired
}

export default ActorList
