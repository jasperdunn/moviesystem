import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { create } from './ActorDataAccess'
import Button from '../shared/Button'
import TextFieldWithLabel from '../shared/TextFieldWithLabel'
import DatePicker from '../shared/DatePicker'
import Page from '../shared/Page'
import Form from '../shared/Form'
import ResponsiveDiv from '../shared/ResponsiveDiv'

class ActorCreate extends Component {
  state = {
    name: '',
    dateOfBirth: new Date().toISOString().split('T')[0],
    redirect: false
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value })
  }

  onCreate = event => {
    event.preventDefault()
    const actor = {
      name: this.state.name,
      dateOfBirth: this.state.dateOfBirth
    }
    create(actor)
      .then(() => this.setState({ redirect: true }))
      .catch(error => console.log(error))
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/actors" />
    }
    return (
      <Page title="Create Actor">
        <ResponsiveDiv>
          <Form onSubmit={this.onCreate} title="Create Actor">
            <TextFieldWithLabel
              name="name"
              label="Name"
              value={this.state.name}
              onChange={this.onChange}
              required
            />
            <DatePicker
              name="dateOfBirth"
              label="Date of birth"
              value={this.state.dateOfBirth}
              onChange={this.onChange}
            />
            <Link to="/actors">Cancel</Link>
            <Button type="submit">Create</Button>
          </Form>
        </ResponsiveDiv>
      </Page>
    )
  }
}

export default ActorCreate
