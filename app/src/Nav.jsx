import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import color from './shared/color'

const Nav = props => (
  <StyledNav>
    <Ul>
      <Li>
        <Link exact to="/movies" aria-label="movies">
          Movies
        </Link>
      </Li>
      <Li>
        <Link exact to="/directors" aria-label="directors">
          Directors
        </Link>
      </Li>
      <Li>
        <Link exact to="/actors" aria-label="actors">
          Actors
        </Link>
      </Li>
    </Ul>
  </StyledNav>
)

const Ul = styled.ul`
  display: flex;
  margin: 0;
  padding: 0;
  width: 100vw;
`

const Li = styled.li`
  display: inline-flex;
  flex: 1;
`

const activeClassName = 'active-nav-link'

const Link = styled(NavLink).attrs({ activeClassName })`
  width: 100%;
  line-height: 48px;
  text-decoration: none;
  color: black;
  text-align: center;
  background-color: ${color.lighter};

  &:hover,
  &.${activeClassName} {
    background-color: ${color.lightest};
  }

  &:active,
  &:active.${activeClassName} {
    background-color: ${color.lighterStill};
  }
`

const StyledNav = styled.nav`
  position: fixed;
  z-index: 1;
`

export default Nav
