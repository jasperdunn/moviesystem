﻿using System.Collections.Generic;
using System.Linq;
using API.Actors;
using Moq;
using Xunit;
using System;

namespace Actors
{
    public class ActorMapperUnitTests
    {
        ActorMapper sut;

        public ActorMapperUnitTests()
        {
            this.sut = new ActorMapper();
        }

        [Fact]
        public void MapEntityToDto()
        {
            var dateOfBirth = new DateTime(1993, 07, 11);

            var entity = new Actor
            {
                ActorId = 1,
                Name = "Actor",
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };

            var expectedResult = new ActorDto
            {
                ActorId = 1,
                Name = "Actor",
                DateOfBirth = "1993-07-11"
            };

            ActorDto actualResult = sut.MapEntityToDto(entity);

            Assert.Equal(actualResult.ActorId, expectedResult.ActorId);
            Assert.Equal(actualResult.Name, expectedResult.Name);
            Assert.Equal(actualResult.DateOfBirth, expectedResult.DateOfBirth);
        }

        [Fact]
        public void MapDtoToEntity()
        {
            var dateOfBirth = new DateTime(1993, 07, 11);

            var dto = new ActorDto
            {
                ActorId = 1,
                Name = "Actor",
                DateOfBirth = "1993-07-11"
            };

            var expectedResult = new Actor
            {
                ActorId = 1,
                Name = "Actor",
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };

            Actor actualResult = sut.MapDtoToEntity(dto);

            Assert.Equal(actualResult.ActorId, expectedResult.ActorId);
            Assert.Equal(actualResult.Name, expectedResult.Name);
            Assert.Equal(actualResult.DateOfBirth, expectedResult.DateOfBirth);
        }
    }
}