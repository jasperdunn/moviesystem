﻿using System.Collections.Generic;
using System.Linq;
using API.Movies;
using API.Omdb;
using Moq;
using Xunit;

namespace Omdb
{
    public class OmdbMapperUnitTests
    {
        OmdbMapper sut;

        public OmdbMapperUnitTests()
        {
            this.sut = new OmdbMapper();
        }

        [Fact]
        public void MapOmdbDataToMovies()
        {
            var movies = new List<Movie>
            {
                new Movie()
                {
                    ImdbId = "123abc"
                },
                new Movie(),
                new Movie()
                {
                    ImdbId = "234abc"
                }
            };

            var omdbData = new List<Movie>
            {
                new Movie()
                {
                    ImdbId = "123abc",
                    Rating = 10d,
                    Runtime = "120 min"
                },
                new Movie()
                {
                    ImdbId = "234abc",
                    Rating = 0d,
                    Runtime = "60 min"
                }
            };

            var expectedResult = new List<Movie>
            {
                new Movie()
                {
                    ImdbId = "123abc",
                    Rating = 10d,
                    Runtime = "120 min"
                },
                new Movie(),
                new Movie()
                {
                    ImdbId = "234abc",
                    Rating = 0d,
                    Runtime = "60 min"
                }
            };

            List<Movie> actualResult = sut.MapOmdbDataToMovies(omdbData, movies);

            Assert.Equal(actualResult[0].Rating, expectedResult[0].Rating);
            Assert.Equal(actualResult[0].Runtime, expectedResult[0].Runtime);
            Assert.Equal(actualResult[1].Rating, expectedResult[1].Rating);
            Assert.Equal(actualResult[1].Runtime, expectedResult[1].Runtime);
            Assert.Equal(actualResult[2].Rating, expectedResult[2].Rating);
            Assert.Equal(actualResult[2].Runtime, expectedResult[2].Runtime);
        }
    }
}