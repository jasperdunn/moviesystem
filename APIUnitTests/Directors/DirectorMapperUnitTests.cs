﻿using System.Collections.Generic;
using System.Linq;
using API.Directors;
using Moq;
using Xunit;
using System;

namespace Directors
{
    public class DirectorMapperUnitTests
    {
        DirectorMapper sut;

        public DirectorMapperUnitTests()
        {
            this.sut = new DirectorMapper();
        }

        [Fact]
        public void MapEntityToDto()
        {
            var dateOfBirth = new DateTime(1993, 07, 11);

            var entity = new Director
            {
                DirectorId = 1,
                Name = "Director",
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };

            var expectedResult = new DirectorDto
            {
                DirectorId = 1,
                Name = "Director",
                DateOfBirth = "1993-07-11"
            };

            DirectorDto actualResult = sut.MapEntityToDto(entity);

            Assert.Equal(actualResult.DirectorId, expectedResult.DirectorId);
            Assert.Equal(actualResult.Name, expectedResult.Name);
            Assert.Equal(actualResult.DateOfBirth, expectedResult.DateOfBirth);
        }

        [Fact]
        public void MapDtoToEntity()
        {
            var dateOfBirth = new DateTime(1993, 07, 11);

            var dto = new DirectorDto
            {
                DirectorId = 1,
                Name = "Director",
                DateOfBirth = "1993-07-11"
            };

            var expectedResult = new Director
            {
                DirectorId = 1,
                Name = "Director",
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };

            Director actualResult = sut.MapDtoToEntity(dto);

            Assert.Equal(actualResult.DirectorId, expectedResult.DirectorId);
            Assert.Equal(actualResult.Name, expectedResult.Name);
            Assert.Equal(actualResult.DateOfBirth, expectedResult.DateOfBirth);
        }
    }
}