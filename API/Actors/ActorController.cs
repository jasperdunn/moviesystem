﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace API.Actors
{
    [ApiController]
    [Route("actors")]
    public class ActorController : ControllerBase
    {
        private IActorRepository actorRepository;
        private IActorMapper actorMapper;

        public ActorController(IActorRepository actorRepository, IActorMapper actorMapper)
        {
            this.actorRepository = actorRepository;
            this.actorMapper = actorMapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody] ActorDto model)
        {
            Actor actor = actorMapper.MapDtoToEntity(model);

            actorRepository.Create(actor);

            return Created(HttpContext.Request.Path + "/" + actor.ActorId, actor);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<Actor> actors = actorRepository.GetAll();

            List<ActorDto> actorDtos = actors.ConvertAll(actor => actorMapper.MapEntityToDto(actor));

            return Ok(actorDtos);
        }

        [HttpGet("{actorId:int}")]
        public IActionResult GetSingle(int actorId)
        {
            Actor actor = actorRepository.GetSingle(actorId);

            if (actor == null)
            {
                return NotFound();
            }

            ActorDto actorDto = actorMapper.MapEntityToDto(actor);

            return Ok(actorDto);
        }

        [HttpPut]
        public IActionResult Update([FromBody] ActorDto model)
        {
            bool exists = actorRepository.Exists(model.ActorId);

            if (!exists)
            {
                return NotFound();
            }

            Actor actor = actorMapper.MapDtoToEntity(model);

            actorRepository.Update(actor);

            return NoContent();
        }

        [HttpDelete("{actorId:int}")]
        public IActionResult Delete(int actorId)
        {
            Actor actor = actorRepository.GetSingle(actorId);

            if (actor == null)
            {
                return NotFound();
            }

            actorRepository.Delete(actor);

            return NoContent();
        }
    }
}
