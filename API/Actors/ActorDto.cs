using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Movies;

namespace API.Actors
{
    public class ActorDto
    {
        public int ActorId { get; set; }
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}