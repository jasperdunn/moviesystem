using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using API.Shared;

namespace API.Actors
{
    public class ActorRepository : IActorRepository
    {
        private IEntityFrameworkOrm<Actor> actorOrm;

        public ActorRepository(IEntityFrameworkOrm<Actor> actorOrm)
        {
            this.actorOrm = actorOrm;
        }

        public void Create(Actor actor)
        {
            actorOrm.Create(actor);
        }

        public List<Actor> GetAll()
        {
            return actorOrm.GetAll().ToList();
        }

        public Actor GetSingle(int actorId)
        {
            return actorOrm.GetSingle(a => a.ActorId == actorId);
        }

        public void Update(Actor actor)
        {
            actorOrm.Update(actor);
        }

        public void Delete(Actor actor)
        {
            actorOrm.Delete(actor);
        }

        public bool Exists(int actorId)
        {
            return actorOrm.Exists(a => a.ActorId == actorId);
        }
    }

    public interface IActorRepository
    {
        void Create(Actor actor);
        List<Actor> GetAll();
        Actor GetSingle(int actorId);
        void Update(Actor actor);
        void Delete(Actor actor);
        bool Exists(int actorId);
    }
}