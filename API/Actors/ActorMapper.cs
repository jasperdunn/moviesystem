using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using API.Shared;

namespace API.Actors
{
    public class ActorMapper : IActorMapper
    {
        public Actor MapDtoToEntity(ActorDto dto)
        {
            DateTime dateOfBirth = DateTime.Parse(dto.DateOfBirth);

            return new Actor
            {
                ActorId = dto.ActorId,
                Name = dto.Name,
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };
        }

        public ActorDto MapEntityToDto(Actor entity)
        {
            return new ActorDto
            {
                ActorId = entity.ActorId,
                Name = entity.Name,
                DateOfBirth = entity.DateOfBirth.ToString("yyyy-MM-dd")
            };
        }
    }

    public interface IActorMapper
    {
        Actor MapDtoToEntity(ActorDto dto);
        ActorDto MapEntityToDto(Actor entity);
    }
}