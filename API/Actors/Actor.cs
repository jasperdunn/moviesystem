using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Movies;
using JoinTables;

namespace API.Actors
{
    public class Actor
    {
        public int ActorId { get; set; }
        [Required]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }

        public virtual ICollection<ActorMovie> ActorMovies { get; set; }
    }
}