using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Omdb;
using API.Movies;

namespace API.Omdb
{
    public class OmdbMapper : IOmdbMapper
    {
        public Movie MapOmdbDataToMovie(Movie omdbData, Movie movie)
        {
            movie.Rating = omdbData.Rating;
            movie.Runtime = omdbData.Runtime;

            return movie;
        }

        public List<Movie> MapOmdbDataToMovies(List<Movie> omdbData, List<Movie> movies)
        {
            List<Movie> mappedMovies = movies.Select(movie =>
            {
                Movie movieToBeMapped = omdbData.FirstOrDefault(x => x.ImdbId == movie.ImdbId);
                if (movieToBeMapped != null)
                {
                    movie.Rating = movieToBeMapped.Rating;
                    movie.Runtime = movieToBeMapped.Runtime;
                }
                return movie;
            }).ToList();

            return mappedMovies;
        }
    }

    public interface IOmdbMapper
    {
        Movie MapOmdbDataToMovie(Movie omdbData, Movie movie);
        List<Movie> MapOmdbDataToMovies(List<Movie> omdbData, List<Movie> movies);
    }
}