using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Omdb;
using API.Movies;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace API.Omdb
{
    public class OmdbApi : IOmdbApi
    {
        private IConfiguration configuration;

        public OmdbApi(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<Movie> GetOmdbData(string imdbId)
        {
            string apiKey = configuration.GetValue<string>("ApiKeys:Omdb");

            Movie movie = new Movie() { ImdbId = imdbId };

            var request = new HttpRequestMessage(HttpMethod.Get, $"http://www.omdbapi.com/?apikey={apiKey}&i={imdbId}&type=movie");

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.SendAsync(request);

            OmdbResult result = response.Content.ReadAsAsync<OmdbResult>().Result;

            if (response.IsSuccessStatusCode)
            {
                movie.Rating = result.ImdbRating;
                movie.Runtime = result.Runtime;
            }

            return movie;
        }
    }

    public interface IOmdbApi
    {
        Task<Movie> GetOmdbData(string imdbId);
    }
}