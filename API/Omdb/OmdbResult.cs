namespace API.Omdb
{
    public class OmdbResult
    {
        public double ImdbRating { get; set; }
        public string Runtime { get; set; }
    }
}