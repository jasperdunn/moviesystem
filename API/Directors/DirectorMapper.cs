using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using API.Shared;

namespace API.Directors
{
    public class DirectorMapper : IDirectorMapper
    {
        public Director MapDtoToEntity(DirectorDto dto)
        {
            DateTime dateOfBirth = DateTime.Parse(dto.DateOfBirth);

            return new Director
            {
                DirectorId = dto.DirectorId,
                Name = dto.Name,
                DateOfBirth = DateTime.SpecifyKind(dateOfBirth, DateTimeKind.Utc)
            };
        }

        public DirectorDto MapEntityToDto(Director entity)
        {
            return new DirectorDto
            {
                DirectorId = entity.DirectorId,
                Name = entity.Name,
                DateOfBirth = entity.DateOfBirth.ToString("yyyy-MM-dd"),
                Movies = entity.Movies
            };
        }
    }

    public interface IDirectorMapper
    {
        Director MapDtoToEntity(DirectorDto dto);
        DirectorDto MapEntityToDto(Director entity);
    }
}