using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.Movies;

namespace API.Directors
{
    public class Director
    {
        public int DirectorId { get; set; }
        [Required]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}