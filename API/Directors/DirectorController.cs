﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace API.Directors
{
    [ApiController]
    [Route("directors")]
    public class DirectorController : ControllerBase
    {
        private IDirectorRepository directorRepository;
        private IDirectorMapper directorMapper;

        public DirectorController(IDirectorRepository directorRepository, IDirectorMapper directorMapper)
        {
            this.directorRepository = directorRepository;
            this.directorMapper = directorMapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody] DirectorDto model)
        {
            Director director = directorMapper.MapDtoToEntity(model);

            directorRepository.Create(director);

            return Created(HttpContext.Request.Path + "/" + director.DirectorId, director);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<Director> directors = directorRepository.GetAll();

            List<DirectorDto> directorDtos = directors.ConvertAll(director =>
                directorMapper.MapEntityToDto(director));

            return Ok(directorDtos);
        }

        [HttpGet("{directorId:int}")]
        public IActionResult GetSingle(int directorId)
        {
            Director director = directorRepository.GetSingle(directorId);

            if (director == null)
            {
                return NotFound();
            }

            DirectorDto directorDto = directorMapper.MapEntityToDto(director);

            return Ok(directorDto);
        }

        [HttpPut]
        public IActionResult Update([FromBody] DirectorDto model)
        {
            bool exists = directorRepository.Exists(model.DirectorId);

            if (!exists)
            {
                return NotFound();
            }

            Director director = directorMapper.MapDtoToEntity(model);

            directorRepository.Update(director);

            return NoContent();
        }

        [HttpDelete("{directorId:int}")]
        public IActionResult Delete(int directorId)
        {
            Director director = directorRepository.GetSingle(directorId);

            if (director == null)
            {
                return NotFound();
            }

            directorRepository.Delete(director);

            return NoContent();
        }
    }
}
