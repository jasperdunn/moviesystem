using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using API.Shared;

namespace API.Directors
{
    public class DirectorRepository : IDirectorRepository
    {
        private IEntityFrameworkOrm<Director> directorOrm;

        public DirectorRepository(IEntityFrameworkOrm<Director> directorOrm)
        {
            this.directorOrm = directorOrm;
        }

        public void Create(Director director)
        {
            directorOrm.Create(director);
        }

        public List<Director> GetAll()
        {
            return directorOrm.GetAll().ToList();
        }

        public Director GetSingle(int directorId)
        {
            return directorOrm.GetSingle(d => d.DirectorId == directorId);
        }

        public void Update(Director director)
        {
            directorOrm.Update(director);
        }

        public void Delete(Director director)
        {
            directorOrm.Delete(director);
        }

        public bool Exists(int directorId)
        {
            return directorOrm.Exists(d => d.DirectorId == directorId);
        }
    }

    public interface IDirectorRepository
    {
        void Create(Director director);
        List<Director> GetAll();
        Director GetSingle(int directorId);
        void Update(Director director);
        void Delete(Director director);
        bool Exists(int directorId);
    }
}