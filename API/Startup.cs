﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using API.Shared;
using API.Movies;
using Microsoft.EntityFrameworkCore;
using API.Directors;
using API.Actors;
using API.Omdb;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddDbContext<MovieSystemDbContext>(
                options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
            );

            services.AddScoped<IEntityFrameworkOrm<Movie>, EntityFrameworkOrm<Movie>>();
            services.AddScoped<IEntityFrameworkOrm<Director>, EntityFrameworkOrm<Director>>();
            services.AddScoped<IEntityFrameworkOrm<Actor>, EntityFrameworkOrm<Actor>>();

            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton(Configuration);

            services.AddTransient<IMovieRepository, MovieRepository>();
            services.AddTransient<IMovieService, MovieService>();

            services.AddTransient<IDirectorRepository, DirectorRepository>();
            services.AddTransient<IDirectorMapper, DirectorMapper>();

            services.AddTransient<IActorRepository, ActorRepository>();
            services.AddTransient<IActorMapper, ActorMapper>();

            services.AddTransient<IOmdbApi, OmdbApi>();
            services.AddTransient<IOmdbMapper, OmdbMapper>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            }

            app.UseMvc();
        }
    }
}
