﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace API.Movies
{
    [ApiController]
    [Route("movies")]
    public class MovieController : ControllerBase
    {
        private IMovieRepository movieRepository;
        private IMovieService movieService;

        public MovieController(IMovieRepository movieRepository, IMovieService movieService)
        {
            this.movieRepository = movieRepository;
            this.movieService = movieService;
        }

        [HttpPost]
        public IActionResult Create([FromBody] Movie model)
        {
            var movie = new Movie
            {
                Title = model.Title,
                Year = model.Year,
                Genre = model.Genre,
                ImdbId = model.ImdbId
            };

            movieRepository.Create(movie);

            return Created(HttpContext.Request.Path + "/" + movie.MovieId, movie);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            List<Movie> movies = movieService.GetAllWithOmdbData();

            return Ok(movies);
        }

        [HttpGet("{movieId:int}")]
        public IActionResult GetSingle(int movieId)
        {
            Movie movie = movieService.GetSingleWithOmdbData(movieId);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(movie);
        }

        [HttpPut]
        public IActionResult Update([FromBody] Movie model)
        {
            Movie movie = movieRepository.GetSingle(model.MovieId);

            if (movie == null)
            {
                return NotFound();
            }

            movie.Title = model.Title;
            movie.Year = model.Year;
            movie.Genre = model.Genre;
            movie.ImdbId = model.ImdbId;

            movieRepository.Update(movie);

            return NoContent();
        }

        [HttpDelete("{movieId:int}")]
        public IActionResult Delete(int movieId)
        {
            Movie movie = movieRepository.GetSingle(movieId);

            if (movie == null)
            {
                return NotFound();
            }

            movieRepository.Delete(movie);

            return NoContent();
        }
    }
}
