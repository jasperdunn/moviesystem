using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using API.Shared;

namespace API.Movies
{
    public class MovieRepository : IMovieRepository
    {
        private IEntityFrameworkOrm<Movie> movieOrm;

        public MovieRepository(IEntityFrameworkOrm<Movie> movieOrm)
        {
            this.movieOrm = movieOrm;
        }

        public void Create(Movie movie)
        {
            movieOrm.Create(movie);
        }

        public List<Movie> GetAll()
        {
            return movieOrm.GetAll().ToList();
        }

        public Movie GetSingle(int movieId)
        {
            return movieOrm.GetSingle(m => m.MovieId == movieId);
        }

        public void Update(Movie movie)
        {
            movieOrm.Update(movie);
        }

        public void Delete(Movie movie)
        {
            movieOrm.Delete(movie);
        }
    }

    public interface IMovieRepository
    {
        void Create(Movie movie);
        List<Movie> GetAll();
        Movie GetSingle(int movieId);
        void Update(Movie movie);
        void Delete(Movie movie);
    }
}