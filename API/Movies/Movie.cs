using System.Collections.Generic;
using API.Directors;
using API.Actors;
using System.ComponentModel.DataAnnotations;
using JoinTables;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace API.Movies
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        public string Title { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
        public string ImdbId { get; set; }

        public virtual Director Director { get; set; }
        public virtual ICollection<ActorMovie> ActorMovies { get; set; }

        [NotMapped]
        public double? Rating { get; set; }
        [NotMapped]
        public string Runtime { get; set; }
    }
}