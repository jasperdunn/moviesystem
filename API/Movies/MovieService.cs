using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Omdb;
using Microsoft.AspNetCore.Mvc;

namespace API.Movies
{
    public class MovieService : IMovieService
    {
        private IMovieRepository movieRepository;
        private IOmdbApi omdbApi;
        private IOmdbMapper omdbMapper;

        public MovieService(IMovieRepository movieRepository, IOmdbApi omdbApi, IOmdbMapper omdbMapper)
        {
            this.movieRepository = movieRepository;
            this.omdbApi = omdbApi;
            this.omdbMapper = omdbMapper;
        }

        public Movie GetSingleWithOmdbData(int movieId)
        {
            Movie movie = movieRepository.GetSingle(movieId);

            if (movie.ImdbId != "")
            {
                Movie omdbData = omdbApi.GetOmdbData(movie.ImdbId).Result;

                movie = omdbMapper.MapOmdbDataToMovie(omdbData, movie);
            }

            return movie;
        }

        public List<Movie> GetAllWithOmdbData()
        {
            List<Movie> movies = movieRepository.GetAll();

            List<Movie> omdbData = movies
                .Where(movie => movie.ImdbId != "")
                .Select(movie => omdbApi.GetOmdbData(movie.ImdbId).Result).ToList();

            List<Movie> moviesWithOmdbData = omdbMapper.MapOmdbDataToMovies(omdbData, movies);

            return moviesWithOmdbData;
        }
    }

    public interface IMovieService
    {
        Movie GetSingleWithOmdbData(int movieId);
        List<Movie> GetAllWithOmdbData();
    }
}