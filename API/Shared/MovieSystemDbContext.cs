using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using API.Movies;
using API.Directors;
using API.Actors;
using JoinTables;

namespace API.Shared
{
    public class MovieSystemDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Actor> Actors { get; set; }

        public MovieSystemDbContext(DbContextOptions<MovieSystemDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActorMovie>()
                .HasKey(joinTable => new { joinTable.ActorId, joinTable.MovieId });

            modelBuilder.Entity<ActorMovie>()
                .HasOne(joinTable => joinTable.Actor)
                .WithMany(actor => actor.ActorMovies)
                .HasForeignKey(joinTable => joinTable.ActorId);

            modelBuilder.Entity<ActorMovie>()
                .HasOne(joinTable => joinTable.Movie)
                .WithMany(movie => movie.ActorMovies)
                .HasForeignKey(joinTable => joinTable.MovieId);
        }
    }
}