using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace API.Shared
{
    public class EntityFrameworkOrm<T> : IEntityFrameworkOrm<T> where T : class
    {
        private MovieSystemDbContext dbContext;
        private DbSet<T> dbSet;

        public EntityFrameworkOrm(MovieSystemDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = dbContext.Set<T>();
        }

        public void Create(T entity)
        {
            dbSet.Add(entity);
            dbContext.SaveChanges();
        }

        public IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        public T GetSingle(Expression<Func<T, bool>> predicate)
        {
            return dbContext.Set<T>().FirstOrDefault(predicate);
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> predicate)
        {
            return dbContext.Set<T>().Where(predicate);
        }

        public void Update(T entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            dbContext.SaveChanges();
        }

        public bool Exists(Expression<Func<T, bool>> predicate)
        {
            return dbContext.Set<T>().Any(predicate);
        }
    }

    public interface IEntityFrameworkOrm<T>
    {
        void Create(T entity);
        IEnumerable<T> GetAll();
        T GetSingle(Expression<Func<T, bool>> predicate);
        IQueryable<T> Query(Expression<Func<T, bool>> predicate);
        void Update(T entity);
        void Delete(T entity);
        bool Exists(Expression<Func<T, bool>> predicate);
    }
}

