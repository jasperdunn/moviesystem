To run the backend (API)
- go to the API folder and replace the connection string in appsettings.json with your own, or create a database with the same name in mssql server.
- in the API directory, open a terminal and execute the command: dotnet run

To run the tests (APIUnitTests)
- in the APIUnitTests directory, open a terminal and execute the command: dotnet test

To run the frontend (app)
- in the app directory, open a terminal and execute the command: yarn
- after the node_modules have finished downloading -> execute the command yarn start

